#!/usr/bin/env python

import sys, time
import daemon
import webui 

class LightshowDaemon(daemon.Daemon):
    def run(self):
        webui.synchronized_lights.hc.initialize()

        while True:
            webui.app.run(webui.slc.port)
            time.sleep(.01)

if __name__ == "__main__":
    lp_daemon = LightshowDaemon(webui.env + '/daemon-lightshowpi.pid')

    if len(sys.argv) == 2:
        if 'start' == sys.argv[1]:
            del sys.argv[1:2]
            lp_daemon.start()
        elif 'stop' == sys.argv[1]:
            lp_daemon.stop()
        elif 'restart' == sys.argv[1]:
            lp_daemon.restart()
        else:
            print "Unknown command"
            sys.exit(2)
    else:
        print "usage: %s start|stop|restart" % sys.argv[0]
        sys.exit(2)
