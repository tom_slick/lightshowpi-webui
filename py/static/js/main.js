var selectedsong;
var settingsObj;
var currentAudio;
var selectedElement;
var lOn = false;
var lOff = true;
var lightsState = 0;
var audioInMode = false;
var j = 0;
var noConnection = false;

function getPlaylists() {
    $.ajax({
        type: 'POST',
        url: '/ajax',
        data: 'option=10',
        dataType: 'json',
        async: true,
        success: function (data) {
            refreshPlaylistUL(data);
        }
    });
}

function getSongs() {
    $.ajax({
        type: 'POST',
        url: '/ajax',
        data: 'option=8',
        dataType: 'json',
        async: true,
        success: function (data) {
            var temp = '';
            for (var i = 0; i < data.songs.length; i++) {
                temp += "<li data-theme='d'><a class='song' href='#' id=" + data.songs[i][1] + ">" + data.songs[i][0] + "</a></li>";
            }
            $('#songsul')
                .html(temp);
            try {
                $('#songsul')
                    .listview('refresh');
            } catch (e) {}
        }
    });
}

function refreshPlaylistUL(data) {
    var temp1 = "<li data-theme='b' data-role='list-divider'>Playlists</li>";
    for (var i = 0; i < data.playlists.length; i++) {
        temp1 += "<li data-theme='d'><a class='playlist' href='#' id='" + data.playlists[i][1] + "'>" + data.playlists[i][0] + "</a></li>"
    }
    $('#specificplaylistsul')
        .html(temp1);
    $('#playlistsul')
        .html(temp1);
    try {
        $('#playlistsul')
            .html(temp1)
            .listview('refresh');
    } catch (e) {}
    try {
        $('#popupNewPlaylist')
            .popup('close');
    } catch (e) {}
    try {
        $('#specificplaylistsul')
            .listview('refresh');
    } catch (e) {}
}

function refreshSongUL(data) {
    var temp1 = "<li data-theme='b' data-role='list-divider'>Playlists</li>";
    for (var i = 0; i < data.songs.length; i++) {
        temp1 += "<li data-theme='d'><a class='song' href='#' id='" + data.songs[i][1] + "'>" + data.songs[i][0] + "</a></li>"
    }
    $('#songsul')
        .html(temp1)
        .listview('refresh');
}


function getCurrentTrack() {
    $.ajax({
        type: 'POST',
        url: '/getvars',
        async: true,
        dataType: 'json',
        success: function (data) {
            buttonState(data.lightstate);
            if (noConnection == true) {
                noConnection = false;
                getPlaylists();
                getSongs();
            }
            try {
                $('#popupError')
                    .popup('close');
            } catch (e) {}
            if (data.currentsong != '') {
                $('.currentsong')
                    .html(data.currentsong);
                $('.currentpos')
                    .html(data.currentpos);
                $('.duration')
                    .html(data.duration);
                if (data.duration != 0) {
                    $('.slider')
                        .attr('max', data.duration);
                } else {
                    $('.slider')
                        .attr('max', '100');
                }
                $('.slider')
                    .each(function () {
                        try {
                            $(this)
                                .val(data.currentpos)
                                .slider('refresh');
                        } catch (e) {}
                    });
            }

            var temp = '';
            for (var i = 0; i < data.playlist.length; i++) {
                if (data.playlist[i] == data.playlistplaying) {
                    temp += '<li data-song="' + data.playlist[i] + '" data-icon="audio"><a href="#">' + data.playlist[i] + '</a></li>';
                    currentAudio = data.playlist[i]
                } else {
                    temp += '<li data-song="' + data.playlist[i] + '">' + data.playlist[i] + '</li>';
                }
            }
            $(".playlistpanelul")
                .html(temp);
            $(".playlistpanelul")
                .each(function () {
                    try {
                        $(this)
                            .listview('refresh');;
                    } catch (e) {}
                });
        },
        error: function (a, e, b) {
            if (e == 'error') {
                $('#popupError')
                    .popup('open');
                noConnection = true;
            }
        },
        complete: function () {
            setTimeout(function () {
                getCurrentTrack()
            }, 1000);
        }
    });
}

function buttonState(state) {
    lightsState = parseInt(state);

    if (lightsState == 0 && !lOff) {
        lOff = true;
        lOn = false;
        audioInMode = false;

        $("#lightsOff")
            .attr('data-icon', 'check');
        $("#lightsOff")
            .addClass("ui-icon-check");

        $("#lightsOn")
            .attr('data-icon', 'blank');
        $("#lightsOn")
            .removeClass("ui-icon-check");

        $("#Audioin")
            .attr('data-icon', 'blank');
        $("#Audioin")
            .removeClass("ui-icon-check");

        $("#playallmusic")
            .text("Play All Music");
        $("#playallmusic")
            .removeClass('ui-btn-f');
        $("#playallmusic")
            .addClass('ui-btn-e');

    } else if (lightsState == 1 && !lOn) {
        lOff = false;
        lOn = true;
        audioInMode = false;
        $("#lightsOn")
            .attr('data-icon', 'check');
        $("#lightsOn")
            .addClass("ui-icon-check");

        $("#lightsOff")
            .attr('data-icon', 'blank');
        $("#lightsOff")
            .removeClass("ui-icon-check");

        $("#playallmusic")
            .text("Play All Music");
        $("#playallmusic")
            .removeClass('ui-btn-f');
        $("#playallmusic")
            .addClass('ui-btn-e');

    } else if (lightsState == 2) {
        $("#lightsOn")
            .attr('data-icon', 'blank');
        $("#lightsOn")
            .removeClass("ui-icon-check");

        $("#lightsOff")
            .attr('data-icon', 'blank');
        $("#lightsOff")
            .removeClass("ui-icon-check");
        if (audioInMode) {
            $("#Audioin")
                .attr('data-icon', 'check');
            $("#Audioin")
                .addClass("ui-icon-check");
        } else { //if (!audioInMode){
            $("#playallmusic")
                .text("Stop Playing");
            $("#playallmusic")
                .removeClass('ui-btn-e');
            $("#playallmusic")
                .addClass('ui-btn-f');

            $("#Audioin")
                .attr('data-icon', 'blank');
            $("#Audioin")
                .removeClass("ui-icon-check");
        }
        lOff = false;
        lOn = false;
    }
}

$(document)
    .ready(function () {
        $.ajax({
            type: 'POST',
            url: '/ajax',
            async: true,
            data: 'option=5',
            dataType: 'json',
            success: function (data) {
                //get settings
                settingsObj = JSON.parse(JSON.stringify(data));
                console.log(settingsObj);
                //setup settings page
                var ulVals = '';
                for (temp in settingsObj) {
                    ulVals += '<li><a href="#' + temp + '" data-ajax="false">' + temp + '</a></li>'; //console.log(temp)
                    $('#tabs')
                        .append('<div id="' + temp + '" class="ui-body-d ui-content"></div>');
                    for (temp2 in settingsObj[temp]) {
                        if (settingsObj[temp][temp2] !== null && typeof settingsObj[temp][temp2] === 'object') {
                            $('#' + temp)
                                .append('<div class="ui-field-contain"><label for="' + temp2 + '">' + temp2 + '</label><input name="' + temp2 + '" id="' + temp2 + '" value=' + JSON.stringify(settingsObj[temp][temp2]) + '></div>');
                        } else {
                            $('#' + temp)
                                .append('<div class="ui-field-contain"><label for="' + temp2 + '">' + temp2 + '</label><input name="' + temp2 + '" id="' + temp2 + '" value="' + settingsObj[temp][temp2] + '"></div>');
                        }
                    }
                    $('#' + temp)
                        .append('<button data-inline="true" data-theme="g" class="saveChanges">Save Changes</button>');
                    $('#' + temp)
                        .append('<button data-inline="true" data-theme="f" class="setDefault">Revert to Default</button>');
                    $('#' + temp)
                        .append('<hidden name="section" id="section" value="' + temp + '">');
                }
                $('#tabsul')
                    .html(ulVals);

                //setup lighting controls
                var lights = settingsObj.hardware.gpio_pins.split(",")
                var lightstr = "<li data-theme='b' data-role='list-divider'>Current Light States</li>";
                for (var i = 0; i < lights.length; i++) {
                    lightstr += "<li data-theme='d'><a data-index='" + i + "' class='light' id='light" + i + "'>Light " + i + "</a></li>";
                }
                $('#lightsUl')
                    .html(lightstr);
            }
        });

        $('#lightsUl')
            .on('click', ".light", function () {
                var port = $(this)
                    .data('index');
                if ($(this)
                    .hasClass('ui-btn-d')) {
                    $(this)
                        .removeClass("ui-btn-d")
                        .addClass("ui-btn-e");
                    $.ajax({
                        type: "POST",
                        url: '/ajax',
                        data: {
                            option: 'lightOn',
                            port: port
                        },
                        async: true
                    });
                } else {
                    $(this)
                        .removeClass("ui-btn-e")
                        .addClass("ui-btn-d");
                    $.ajax({
                        type: "POST",
                        url: '/ajax',
                        data: {
                            option: 'lightOff',
                            port: port
                        },
                        async: true
                    });
                }
            });

        $('#uploadfile')
            .on('submit', function (e) {
                e.stopPropagation(); // Stop stuff happening
                e.preventDefault(); // Totally stop stuff happening
                $('progress')
                    .css({
                        visibility: 'visible'
                    });

                var formData = new FormData($(this)[0]);
                $.ajax({
                    url: '/upload', //Server script to process data
                    type: 'POST',
                    data: formData,
                    dataType: 'json',
                    xhr: function () { // Custom XMLHttpRequest
                        var myXhr = $.ajaxSettings.xhr();
                        if (myXhr.upload) { // Check if upload property exists
                            myXhr.upload.addEventListener('progress', progressHandlingFunction, false); // For handling the progress of the upload
                        }
                        return myXhr;
                    },
                    success: function (data) {
                        $('#uploadfile')[0].reset();
                        $('progress')
                            .css({
                                visibility: 'hidden'
                            });
                        $('progress')
                            .attr({
                                value: 0,
                                max: 100
                            });
                        refreshSongUL(data);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert('ERRORS: ' + textStatus);
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
            });

        function progressHandlingFunction(e) {
            if (e.lengthComputable) {
                $('progress')
                    .attr({
                        value: e.loaded,
                        max: e.total
                    });
            }
        }
        $('#tabs')
            .on('click', '.setDefault', function () {
                var a = confirm('Are you sure you want to reset the Config');
                if (a) {
                    $.ajax({
                        type: 'POST',
                        url: '/ajax',
                        async: true,
                        data: 'option=11'
                    });
                }
            });

        $('#tabs')
            .on('click', '.saveChanges', function () {
                var config = {};
                for (obj in settingsObj) {
                    config[obj] = {};
                    $('#' + obj + ' input')
                        .serializeArray()
                        .map(function (item) {
                            config[obj][item.name] = item.value;
                        });
                }
                $.ajax({
                    type: 'POST',
                    url: '/ajax',
                    async: true,
                    data: 'option=6&object=' + JSON.stringify(config)
                });
            });

        $('#songsul')
            .on('click', '.song', function () {
                selectedsong = this.id;
                $('#popupTitle')
                    .html($(this)
                        .html());
                $("#popupSong")
                    .popup('open');
            });

        $('#playNow')
            .click(function () {
                if (lightsState == 2) {
                    $("#popupSong")
                        .popup('close');
                    return;
                }

                $("#popupSong")
                    .popup('close');
                $.ajax({
                    type: 'POST',
                    url: '/ajax',
                    data: 'option=1&song=' + selectedsong,
                    async: true
                });
            });

        $('#playlistsul')
            .on('click', '.playlist', function () {
                selectedsong = this.id;
                $('#popupTitlelist')
                    .html($(this)
                        .html());
                $("#popupPlaylist")
                    .popup('open');
            });

        $('#playNowlist')
            .click(function () {
                if (lightsState == 2) {
                    $("#popupPlaylist")
                        .popup('close');
                    return;
                }

                $("#popupPlaylist")
                    .popup('close');

                $.ajax({
                    type: 'POST',
                    url: '/ajax',
                    data: 'option=0&playlist=' + selectedsong,
                    async: true
                });
            });

        $('#Audioin')
            .click(function () {
                console.log('option 16');
                var display = $("#Audioin")
                    .attr('data-icon');

                if (lightsState == 2 && !audioInMode) {
                    return;
                }
                audioInMode = true;

                $.ajax({
                    type: 'POST',
                    url: '/ajax',
                    data: 'option=16',
                    async: true
                });

            });

        $('#deletelist')
            .click(function () {
                var a = confirm('Are you sure you want to delete this playlist');
                if (a) {
                    $("#popupPlaylist")
                        .popup('close');
                    $.ajax({
                        type: 'POST',
                        url: '/ajax',
                        data: 'option=10&playlist=' + selectedsong,
                        dataType: 'json',
                        async: true,
                        success: function (data) {
                            refreshPlaylistUL(data);
                        }
                    });
                }
            });

        $('#addToQueue')
            .click(function () {
                $("#popupSong")
                    .popup('close');
                $.ajax({
                    type: 'POST',
                    url: '/ajax.php',
                    data: 'option=5&song=' + selectedsong,
                    async: true
                });
            });

        $('#lightsOn')
            .click(function () {
                if (lightsState == 2) {
                    return;
                }

                $.ajax({
                    type: 'POST',
                    url: '/ajax',
                    data: 'option=3',
                    async: true
                });
            });

        $('#tabs')
            .on('click', '.saveChanges', function () {
                var config = {};
                for (obj in settingsObj) {
                    config[obj] = {};
                    $('#' + obj + ' input')
                        .serializeArray()
                        .map(function (item) {
                            config[obj][item.name] = item.value;
                        });
                }
                $.ajax({
                    type: 'POST',
                    url: '/ajax',
                    async: true,
                    data: 'option=6=' + JSON.stringify(config)
                });
            });

        $('#lightsOff')
            .click(function () {
                if (lightsState == 2) {
                    return;
                }

                $.ajax({
                    type: 'POST',
                    url: '/ajax',
                    data: 'option=4',
                    async: true
                });
            });

        $('#restartPi')
            .click(function () {
                $('#popupRestart')
                    .popup('open');
            });

        $('#shutdownPi')
            .click(function () {
                $('#popupShutdown')
                    .popup('open');
            });

        $('#restartPiConfirm')
            .click(function () {
                $('#popupRestart')
                    .popup('close');
                $.ajax({
                    type: 'POST',
                    url: '/ajax',
                    data: 'option=13',
                    async: true
                });
            });

        $('#shutdownPiConfirm')
            .click(function () {
                $('#popupShutdown')
                    .popup('close');
                $.ajax({
                    type: 'POST',
                    url: '/ajax',
                    data: 'option=12',
                    async: true
                });
            });

        $('#playallmusic')
            .click(function () {
                if (lightsState == 2 && audioInMode) {
                    return;
                } else if (lightsState == 2) {
                    $.ajax({
                        type: 'POST',
                        url: '/ajax',
                        data: 'option=2',
                        async: true
                    });
                    return;
                }
                $.ajax({
                    type: 'POST',
                    url: '/ajax',
                    data: 'option=7',
                    async: true
                });
            });

        $("#playlistsongs")
            .on('click', '.songsforplaylist', function () {
                if ($(this)
                    .hasClass('ui-btn-d')) {
                    $(this)
                        .removeClass("ui-btn-d")
                        .addClass("ui-btn-e");
                } else {
                    $(this)
                        .removeClass("ui-btn-e")
                        .addClass("ui-btn-d");
                }
            });

        $('#newplaylistsubmit')
            .click(function () {
                var name = $("#newplaylistname")
                    .val();
                if (name == '') {
                    alert('Please Enter a name');
                    return false;
                }
                var temp = '';
                $('.songsforplaylist.ui-btn-e')
                    .each(function () {
                        temp += $(this)
                            .html()
                            .replace(/\.[^/.]+$/, "") + "\t" + $(this)
                            .data('filename') + "\r\n";
                    });
                $.ajax({
                    type: 'POST',
                    url: '/ajax',
                    data: 'option=9&val=' + temp + '&name=' + name,
                    async: true,
                    dataType: 'json',
                    success: function (data) {
                        refreshPlaylistUL(data);
                    }
                });
            });

        $('#newplaylist')
            .click(function () {
                $.ajax({
                    type: 'POST',
                    url: '/ajax',
                    data: 'option=8',
                    dataType: 'json',
                    async: true,
                    success: function (data) {
                        var temp = '';
                        for (var i = 0; i < data.songs.length; i++) {
                            temp += "<li data-theme='d'><a class='songsforplaylist' href='#' data-filename=\"" + data.songs[i][1] + "\">" + data.songs[i][0] + "</a></li>";
                        }
                        $('#playlistsongs')
                            .html(temp);
                        try {
                            $('#playlistsongs')
                                .listview('refresh');
                        } catch (e) {
                            $('#playlistsongs')
                                .listview();
                        }
                        $('#popupNewPlaylist')
                            .popup('open');
                    }
                });
            });

        $("#then")
            .change(function () {
                if ($(this)
                    .val() == 'playlist') {
                    $('#popupspecifiplaylist')
                        .popup('open');
                }
            });

        $("#specificplaylistsul")
            .on('click', '.playlist', function () {
                $('#specificplaylist')
                    .val(this.id);
                $('#specificplaylist')
                    .data('name', $(this)
                        .html());
                $('#popupspecifiplaylist')
                    .popup('close');
            });

        $('#songs')
            .on('pageshow', function () {
                $('#songsul')
                    .listview('refresh');
            });

        $('#playlists')
            .on('pageshow', function () {
                $('#playlistsul')
                    .listview('refresh');
            });

        getPlaylists();
        getSongs();
    });
