#!/usr/bin/env python

import web
import glob
import os
import synchronized_lights
import json
import sys

template_dir = os.path.abspath(os.path.dirname(__file__)) + "/templates"

slc = synchronized_lights.Lightshow()
env = os.environ['SYNCHRONIZED_LIGHTS_HOME']

urls = (
    '/', 'Index',
    '/index.html', 'Index',
    '/favicon.ico', 'Favicon',
    '/app.manifest', 'Hello',
    '/ajax', 'Ajax',
    '/getvars', 'GetVars',
    '/upload', 'Upload',
    '/music', 'Music',
    '/(js|css|img)/(.*)', 'Static'
)


class Index(object):

    @staticmethod
    def GET():
        return render.index()


class Hello(object):

    @staticmethod
    def GET():
        web.header('Content-Type', 'text/cache-manifest')
        rmod = "r"

        f = open(os.environ['SYNCHRONIZED_LIGHTS_HOME'] + '/py/static/app.manifest', rmod)

        try:
            stream = f.read()
            return stream
        except IOError:
            f.close()
            return '404 Not Found'


class Static(object):

    @staticmethod
    def GET(media, fn):
        rmod = "r"

        if fn.endswith(".png"):
            rmod = "rb"

        f = open(os.environ['SYNCHRONIZED_LIGHTS_HOME'] + '/py/static/' + media + '/' + fn, rmod)

        try:
            stream = f.read()
            return stream
        except IOError:
            f.close()
            return '404 Not Found'


class Favicon(object):

    @staticmethod
    def GET():
        f = open(os.environ['SYNCHRONIZED_LIGHTS_HOME'] + "/py/static/favicon.ico", 'rb')
        return f.read()


class Music(object):

    @staticmethod
    def POST():
        web.header('Content-Type', 'application/json')
        return '{"sr":' + str(slc.sr) + ',"nc":' + str(slc.nc) + ',"fc":' + str(slc.fc) + '}'

    @staticmethod
    def GET():
        return slc.thedata


class Ajax(object):

    @staticmethod
    def GET():
        var = web.input()
        if var.option == '1':
            return slc.audioChunkNumber

    @staticmethod
    def POST():
        variables = web.input()

        if variables.option == '0':
            slc.play_playlist(variables.playlist)

        elif variables.option == '1':
            slc.play_single(variables.song)

        elif variables.option == '2':
            slc.stop()

        elif variables.option == '3':
            slc.lightson()

        elif variables.option == '4':
            slc.lightsoff()

        elif variables.option == 'lightOn':
            # turn on a light
            slc.lighton(int(variables.port))

        elif variables.option == 'lightOff':
            # turn off a light
            slc.lightoff(int(variables.port))

        elif variables.option == '5':
            web.header('Content-Type', 'application/json')
            return slc.get_config()

        elif variables.option == '6':
            slc.set_config(variables.object)

        elif variables.option == '7':
            slc.play_all()

        elif variables.option == '8':
            web.header('Content-Type', 'application/json')
            str1 = '{"songs":['

            for f_nane in glob.glob(env + "/music/*.mp3"):
                str1 = str1 + '["' + os.path.basename(f_nane) + '","' + f_nane + '"],'

            for f_nane in glob.glob(env + "/music/*.wav"):
                str1 = str1 + '["' + os.path.basename(f_nane) + '","' + f_nane + '"],'

            str1 = str1[:-1] + ']}'

            return str1

        elif variables.option == '9':
            web.header('Content-Type', 'application/json')

            with open(env + "/music/playlists/" + variables.name + ".playlist", "w") as file_fp:
                file_fp.write(variables.val)

            str1 = '{"playlists":['

            for f_nane in glob.glob(env + "/music/playlists/*.playlist"):
                str1 = str1 + '["' + os.path.basename(f_nane) + '","' + f_nane + '"],'

            str1 = str1[:-1] + ']}'

            return str1

        elif variables.option == '10':
            web.header('Content-Type', 'application/json')

            if hasattr(variables, 'playlist'):
                os.remove(variables.playlist)

            str1 = '{"playlists":['

            for f_nane in glob.glob(env + "/music/playlists/*.playlist"):
                str1 = str1 + '["' + os.path.basename(f_nane) + '","' + f_nane + '"],'

            str1 = str1[:-1] + ']}'

            return str1

        elif variables.option == '11':
            slc.set_config_default()

        elif variables.option == '12':
            app.stop()
            os.system("sudo shutdown -h now")

        elif variables.option == '13':
            app.stop()
            os.system("sudo shutdown -r now")

        elif variables.option == '14':
            return slc.audio_chunk

        elif variables.option == '16':
            if slc.audio_in_mode:
                slc.audio_in_mode = False
            else:
                slc.audio_in_mode = True
                slc.audio_in()


class GetVars(object):

    @staticmethod
    def POST():
        web.header('Content-Type', 'application/json')
        str1 = ''

        for temp in slc.current_playlist:
            str1 = str1 + '"' + temp[0] + '",'

        str1 = str1[:-1]

        return '{"currentsong":"' + slc.current_song_name + '","duration":"' + str(
            slc.duration) + '","currentpos":"' + str(
            slc.current_position) + '","playlist":[' + str1 + '],"playlistplaying":"' +\
            slc.playlistplaying + '","lightstate":"' + str(slc.lights_state) + '"}'


class Upload(object):

    @staticmethod
    def POST():
        # change this to the directory you want to store the file in.
        filedir = env + "/music/"
        i = web.webapi.rawinput()
        files = i.myfile
        if not isinstance(files, list):
            files = [files]

        for x in files:
            # replaces the windows-style slashes with linux ones.
            # filepath = x.filename.replace('\\', '/')
            # splits the and chooses the last part (the filename with extension)
            # filename = filepath.split('/')[-1]
            # creates the file where the uploaded file should be stored

            f_name = ''.join(os.path.basename(x.filename).split())
            with open(filedir + f_name, 'w') as fout:

                # writes the uploaded file to the newly created file.
                fout.write(x.file.read())

        web.header('Content-Type', 'application/json')
        str1 = '{"songs":['
        
        for f_name in glob.glob(env + "/music/*.mp3"):
            str1 = str1 + '["' + os.path.basename(f_name) + '","' + f_name + '"],'
            
        str1 = str1[:-1] + ']}'

        return str1


class Application(web.application):
    def run(self, port=8080, *middleware):
        func = self.wsgifunc(*middleware)

        return web.httpserver.runsimple(func, ('0.0.0.0', port))

web.config.debug = False
app = Application(urls, globals())
render = web.template.render(template_dir, cache=True, globals={'glob': glob, 'os': os})

if __name__ == "__main__":
    synchronized_lights.hc.initialize()
    app.run(port=slc.port)

    slc.lightsoff()